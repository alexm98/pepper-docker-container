# Docker pepper container 

based on ```consol/centos-xfce-vnc```

It's supposed you already have choregraphe-suite-2.5.10.7-linux64-setup.run downloaded locally in the directory with this Dockerfile

## The Python script

This image also uses a Python script that automatically adds coregraphe's license key using pyautogui.

## Building the container

```docker build -t "pepper" .```

## Running the container

```docker run -it --user 0 -p 6911:6901 pepper```

## Connecting to the container via VNC 
You can connect to the container with vinagre or other VNC client.

connect with 172.17.0.2:5901 password vncpassword
