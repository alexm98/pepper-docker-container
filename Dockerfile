FROM consol/centos-xfce-vnc

USER 0

RUN yum install -y libXaw python-pip
RUN pip install --upgrade pip && pip install qi xlib

ADD choregraphe-suite-2.5.10.7-linux64-setup.run /root/

RUN chmod +x /root/choregraphe-suite-2.5.10.7-linux64-setup.run
RUN /root/choregraphe-suite-2.5.10.7-linux64-setup.run --mode unattended
RUN rm /root/choregraphe-suite-2.5.10.7-linux64-setup.run

RUN mkdir /Aldebaran\ Robotics
RUN echo "[General]" > /Aldebaran\ Robotics/Choregraphe.conf
RUN echo "LicenseKey=<your-license-key>" >> /Aldebaran\ Robotics/Choregraphe.conf
